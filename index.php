<?php
 
$mysqli = new mysqli('localhost', 'root', 'root', 'assembly_constructor'); // Поменять пароль!
 
if ($mysqli->connect_error) {
     die('Ошибка подключения (' . $mysqli->connect_errno . ') '
      . $mysqli->connect_error);
}
// echo '<p>Соединение установлено... ' . $mysqli->host_info . "</p>";


// Запрос к таблице gpu и вывод в массив $gpu со всеми значениями. 
$query = $mysqli->prepare("SELECT * FROM gpu");
$query->execute();
$gpu = $query->get_result()->fetch_all();
$query->close();
// Конец запроса

// Запрос к таблице и вывод в массив $cpu со всеми значениями. 
$query = $mysqli->prepare("SELECT * FROM cpu");
$query->execute();
$cpu = $query->get_result()->fetch_all();
$query->close();
// Конец запроса

// Запрос к таблице и вывод в массив $motherboard со всеми значениями. 
$query = $mysqli->prepare("SELECT * FROM motherboard");
$query->execute();
$motherboard = $query->get_result()->fetch_all();
$query->close();
// Конец запроса

// Запрос к таблице и вывод в массив $cpu_cooler со всеми значениями. 
$query = $mysqli->prepare("SELECT * FROM cpu_cooler");
$query->execute();
$cpu_cooler = $query->get_result()->fetch_all();
$query->close();
// Конец запроса
// Запрос к таблице и вывод в массив $cpu_water_cooling со всеми значениями. 
$query = $mysqli->prepare("SELECT * FROM cpu_water_cooling");
$query->execute();
$cpu_water_cooling = $query->get_result()->fetch_all();
$query->close();
// Конец запроса

// Запрос к таблице и вывод в массив $ram со всеми значениями. 
$query = $mysqli->prepare("SELECT * FROM ram");
$query->execute();
$ram = $query->get_result()->fetch_all();
$query->close();
// Конец запроса

// Запрос к таблице и вывод в массив $hdd со всеми значениями. 
$query = $mysqli->prepare("SELECT * FROM hdd");
$query->execute();
$hdd = $query->get_result()->fetch_all();
$query->close();
// Конец запроса

// Запрос к таблице и вывод в массив $ssd_sata со всеми значениями. 
$query = $mysqli->prepare("SELECT * FROM ssd_sata");
$query->execute();
$ssd_sata = $query->get_result()->fetch_all();
$query->close();
// Конец запроса

// Запрос к таблице и вывод в массив $ssd_m2 со всеми значениями. 
$query = $mysqli->prepare("SELECT * FROM ssd_m2");
$query->execute();
$ssd_m2 = $query->get_result()->fetch_all();
$query->close();
// Конец запроса

// Запрос к таблице и вывод в массив $power_supply со всеми значениями. 
$query = $mysqli->prepare("SELECT * FROM power_supply");
$query->execute();
$power_supply = $query->get_result()->fetch_all();
$query->close();
// Конец запроса

// Запрос к таблице и вывод в массив $body со всеми значениями. 
$query = $mysqli->prepare("SELECT * FROM body");
$query->execute();
$body = $query->get_result()->fetch_all();
$query->close();
// Конец запроса

// Запрос к таблице и вывод в массив $wi_fi со всеми значениями. 
$query = $mysqli->prepare("SELECT * FROM wi_fi");
$query->execute();
$wi_fi = $query->get_result()->fetch_all();
$query->close();
// Конец запроса

// Запрос к таблице и вывод в массив $cooler со всеми значениями. 
$query = $mysqli->prepare("SELECT * FROM cooler");
$query->execute();
$cooler = $query->get_result()->fetch_all();
$query->close();
// Конец запроса

// $result = $mysqli->query("SHOW TABLES;");
// while ($row = $result->fetch_row()) {
//     echo "<p>Таблица: {$row[0]}</p>";
// }
// echo 'Версия MYSQL сервера: ' . $mysqli->server_info . "\n";
// $mysqli->close();

    require_once "config.php";
    require_once "delete.php";
?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Конструктор ПК</title>

    <!-- Bootstap 5 -->
    <link rel="stylesheet" href="/bootstrap-5.0.2-dist/css/bootstrap.min.css">
    <!-- <script defer src="/bootstrap-5.0.2-dist/js/bootstrap.bundle.min.js"></script> -->
    <!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" 
    rel="stylesheet" 
    integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" 
    crossorigin="anonymous"> -->

    <!-- Стили -->
    <link rel="stylesheet" type="text/css" href="/css/custom.css">

</head>
<body>

    <header>
        <nav class="navbar navbar-expand-lg bg-body-tertiary" style="background-color: #dedede;">
            <div class="container-fluid">
                <a href="index.php" class="navbar-brand" style="color: black;">
                    <img src="/resource/logo.png" alt="Logo" width="30" height="30" class="d-inline-block align-text-top">
                    Конструктор ПК
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Переключатель навигации">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                    <div class="navbar-nav">
                        <a class="nav-link navigation" href="main.php">Главная</a>
                        <a class="nav-link navigation" href="index.php">Конструктор</a>
                    </div>
                </div>
            </div>
        </nav>
    </header>
    <main class="content">
        <div class="conf-container" style="display: block;">
            <div class="col-xs-12 col-md-9">
                <div class="list-group" id="steps_groups">
                    <div class="list-group-item parent">
                        <h3>Комплектующие:</h3>
                        <div class="list-group steps">
                            <div class="list-group-item modal-link">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-1">
			                            <div class="picture original">
								            <img src="/resource/gpu.png" class="img-responsive">
							            </div>
		                            </div>
                                    <div class="col-xs-12 col-sm-9">
			                            <h4>Видеокарта</h4>
		                            </div>
                                    <div class="col-xs-12 col-sm-2 text-center text-md-right">
										<button type="button" class="btn btn-primary btn-ajax" data-bs-toggle="modal" data-bs-target="#modal-gpu" title="Выбрать">
				                            <i class="fa fa-plus" aria-hidden="true">+</i>
							            </button>
			                            <button class="btn btn-danger btn-delete hidden">
				                            <i class="fa fa-trash-o" aria-hidden="true">-</i>
							            </button>
					                </div>
                                </div>
                                <div class="products deleted">
                                    <?php 
                                        
                                    ?>
                                </div>
                            </div>
                            <div class="list-group-item modal-link">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-1">
			                            <div class="picture original">
								            <img src="/resource/cpu.png" class="img-responsive">
							            </div>
		                            </div>
                                    <div class="col-xs-12 col-sm-9">
			                            <h4>Процессор
                                            <small>*обязательный шаг</small>
                                        </h4>
		                            </div>
                                    <div class="col-xs-12 col-sm-2 text-center text-md-right">
										<button class="btn btn-primary btn-ajax" data-bs-toggle="modal" data-bs-target="#modal-cpu" title="Выбрать">
				                            <i class="fa fa-plus" aria-hidden="true">+</i>
							            </button>
			                            <button class="btn btn-danger btn-delete hidden">
				                            <i class="fa fa-trash-o" aria-hidden="true">-</i>
							            </button>
					                </div>
                                </div>
                                <div class="products"></div>
                            </div>
                            <div class="list-group-item modal-link">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-1">
			                            <div class="picture original">
								            <img src="/resource/motherboard.png" class="img-responsive">
							            </div>
		                            </div>
                                    <div class="col-xs-12 col-sm-9">
			                            <h4>Материнская плата
                                            <small>*обязательный шаг</small>
                                        </h4>
		                            </div>
                                    <div class="col-xs-12 col-sm-2 text-center text-md-right">
										<button class="btn btn-primary btn-ajax" data-bs-toggle="modal" data-bs-target="#modal-motherboard" title="Выбрать">
				                            <i class="fa fa-plus" aria-hidden="true">+</i>
							            </button>
			                            <button class="btn btn-danger btn-delete hidden">
				                            <i class="fa fa-trash-o" aria-hidden="true">-</i>
							            </button>
					                </div>
                                </div>
                                <div class="products deleted"></div>
                            </div>
                            <div class="list-group-item modal-link">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-1">
			                            <div class="picture original">
								            <img src="/resource/cpu_cooler.png" class="img-responsive">
							            </div>
		                            </div>
                                    <div class="col-xs-12 col-sm-9">
			                            <h4>Кулер для процессора</h4>
		                            </div>
                                    <div class="col-xs-12 col-sm-2 text-center text-md-right">
										<button class="btn btn-primary btn-ajax" data-bs-toggle="modal" data-bs-target="#modal-cpu_cooler" title="Выбрать">
				                            <i class="fa fa-plus" aria-hidden="true">+</i>
							            </button>
			                            <button class="btn btn-danger btn-delete hidden">
				                            <i class="fa fa-trash-o" aria-hidden="true">-</i>
							            </button>
					                </div>
                                </div>
                                <div class="products deleted"></div>
                            </div>
                            <div class="list-group-item modal-link">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-1">
			                            <div class="picture original">
								            <img src="/resource/cpu_water_cooling.png" class="img-responsive">
							            </div>
		                            </div>
                                    <div class="col-xs-12 col-sm-9">
			                            <h4>Водяное охлаждение процессора</h4>
		                            </div>
                                    <div class="col-xs-12 col-sm-2 text-center text-md-right">
										<button class="btn btn-primary btn-ajax" data-bs-toggle="modal" data-bs-target="#modal-cpu_water_cooling" title="Выбрать">
				                            <i class="fa fa-plus" aria-hidden="true">+</i>
							            </button>
			                            <button class="btn btn-danger btn-delete hidden">
				                            <i class="fa fa-trash-o" aria-hidden="true">-</i>
							            </button>
					                </div>
                                </div>
                                <div class="products deleted"></div>
                            </div>
                            <div class="list-group-item modal-link">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-1">
			                            <div class="picture original">
								            <img src="/resource/ram.png" class="img-responsive">
							            </div>
		                            </div>
                                    <div class="col-xs-12 col-sm-9">
			                            <h4>Оперативная память
                                            <small>*обязательный шаг</small>
                                        </h4>
		                            </div>
                                    <div class="col-xs-12 col-sm-2 text-center text-md-right">
										<button class="btn btn-primary btn-ajax" data-bs-toggle="modal" data-bs-target="#modal-ram" title="Выбрать">
				                            <i class="fa fa-plus" aria-hidden="true">+</i>
							            </button>
			                            <button class="btn btn-danger btn-delete hidden">
				                            <i class="fa fa-trash-o" aria-hidden="true">-</i>
							            </button>
					                </div>
                                </div>
                                <div class="products deleted"></div>
                            </div>
                            <div class="list-group-item modal-link">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-1">
			                            <div class="picture original">
								            <img src="/resource/hdd.png" class="img-responsive">
							            </div>
		                            </div>
                                    <div class="col-xs-12 col-sm-9">
			                            <h4>Жесткий диск</h4>
		                            </div>
                                    <div class="col-xs-12 col-sm-2 text-center text-md-right">
										<button class="btn btn-primary btn-ajax" data-bs-toggle="modal" data-bs-target="#modal-hdd" title="Выбрать">
				                            <i class="fa fa-plus" aria-hidden="true">+</i>
							            </button>
			                            <button class="btn btn-danger btn-delete hidden">
				                            <i class="fa fa-trash-o" aria-hidden="true">-</i>
							            </button>
					                </div>
                                </div>
                                <div class="products deleted"></div>
                            </div>
                            <div class="list-group-item modal-link">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-1">
			                            <div class="picture original">
								            <img src="/resource/ssd_sata.png" class="img-responsive">
							            </div>
		                            </div>
                                    <div class="col-xs-12 col-sm-9">
			                            <h4>SSD накопитель</h4>
		                            </div>
                                    <div class="col-xs-12 col-sm-2 text-center text-md-right">
										<button class="btn btn-primary btn-ajax" data-bs-toggle="modal" data-bs-target="#modal-ssd_sata" title="Выбрать">
				                            <i class="fa fa-plus" aria-hidden="true">+</i>
							            </button>
			                            <button class="btn btn-danger btn-delete hidden">
				                            <i class="fa fa-trash-o" aria-hidden="true">-</i>
							            </button>
					                </div>
                                </div>
                                <div class="products deleted"></div>
                            </div>
                            <div class="list-group-item modal-link">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-1">
			                            <div class="picture original">
								            <img src="/resource/ssd_m2.png" class="img-responsive">
							            </div>
		                            </div>
                                    <div class="col-xs-12 col-sm-9">
			                            <h4>SSD M.2</h4>
		                            </div>
                                    <div class="col-xs-12 col-sm-2 text-center text-md-right">
										<button class="btn btn-primary btn-ajax" data-bs-toggle="modal" data-bs-target="#modal-ssd_m2" title="Выбрать">
				                            <i class="fa fa-plus" aria-hidden="true">+</i>
							            </button>
			                            <button class="btn btn-danger btn-delete hidden">
				                            <i class="fa fa-trash-o" aria-hidden="true">-</i>
							            </button>
					                </div>
                                </div>
                                <div class="products deleted"></div>
                            </div>
                            <div class="list-group-item modal-link">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-1">
			                            <div class="picture original">
								            <img src="/resource/power_supply.png" class="img-responsive">
							            </div>
		                            </div>
                                    <div class="col-xs-12 col-sm-9">
			                            <h4>Блок питания
                                            <small>*обязательный шаг</small>
                                        </h4>
		                            </div>
                                    <div class="col-xs-12 col-sm-2 text-center text-md-right">
										<button class="btn btn-primary btn-ajax" data-bs-toggle="modal" data-bs-target="#modal-power_supply" title="Выбрать">
				                            <i class="fa fa-plus" aria-hidden="true">+</i>
							            </button>
			                            <button class="btn btn-danger btn-delete hidden">
				                            <i class="fa fa-trash-o" aria-hidden="true">-</i>
							            </button>
					                </div>
                                </div>
                                <div class="products deleted"></div>
                            </div>
                            <div class="list-group-item modal-link">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-1">
			                            <div class="picture original">
								            <img src="/resource/body.png" class="img-responsive">
							            </div>
		                            </div>
                                    <div class="col-xs-12 col-sm-9">
			                            <h4>Корпус
                                            <small>*обязательный шаг</small>
                                        </h4>
		                            </div>
                                    <div class="col-xs-12 col-sm-2 text-center text-md-right">
										<button class="btn btn-primary btn-ajax" data-bs-toggle="modal" data-bs-target="#modal-body" title="Выбрать">
				                            <i class="fa fa-plus" aria-hidden="true">+</i>
							            </button>
			                            <button class="btn btn-danger btn-delete hidden">
				                            <i class="fa fa-trash-o" aria-hidden="true">-</i>
							            </button>
					                </div>
                                </div>
                                <div class="products deleted"></div>
                            </div>
                            <div class="list-group-item modal-link">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-1">
			                            <div class="picture original">
								            <img src="/resource/wi-fi.png" class="img-responsive">
							            </div>
		                            </div>
                                    <div class="col-xs-12 col-sm-9">
			                            <h4>Wi-Fi адаптер</h4>
		                            </div>
                                    <div class="col-xs-12 col-sm-2 text-center text-md-right">
										<button class="btn btn-primary btn-ajax" data-bs-toggle="modal" data-bs-target="#modal-wi_fi" title="Выбрать">
				                            <i class="fa fa-plus" aria-hidden="true">+</i>
							            </button>
			                            <button class="btn btn-danger btn-delete hidden">
				                            <i class="fa fa-trash-o" aria-hidden="true">-</i>
							            </button>
					                </div>
                                </div>
                                <div class="products deleted">
                                    <?  ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <footer class="footer" id="footer">
        <nav class="navbar navbar-expand-lg bg-body-tertiary" style="background-color: #252525;">
            <div class="container-fluid">
                <a href="index.php" class="navbar-brand" style="color: #ffff;">
                    Конструктор ПК
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Переключатель навигации">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                    <div class="navbar-nav">
                        <a class="nav-link navigation" href="main.php" style="color: #ffff;">Главная</a>
                        <a class="nav-link navigation" href="index.php" style="color: #ffff;">Конструктор</a>
                    </div>
                </div>
            </div>
        </nav>
    </footer>

    <!-- 
    ===========================================================
    МОДАЛЬНЫЕ ОКНА: 
    ===========================================================
    -->
    <!-- Видеокарта -->
    <div class="modal fade webprostor-configirator-modal theme-light in" id="modal-gpu"
            tabindex="-1" aria-labelledby="modalProductsLabel" aria-hidden="true" 
            style="overflow-y: scroll;">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalProductsLabel">Видеокарта</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12 col-md-12">
                            <div class="webprostor-configurator-catalog-section">
                                <div class="row">

                                <? foreach ($gpu as $gpuList): ?> <!-- Цикл for из массива $.... Поле таблицы - $...List[цифра] 
                                    (отсчёт идёт с нуля: 0 - первое поле таблицы, 1 - второе поле и т.д.)  -->

                                    <!-- Карточка товара -->
                                    <div class="col-md-4 col-sm-6 col-xs-12 configurator-product mb-3">
                                        <div class="panel panel-default border rounded" 
                                                style="display: flex; 
                                                    flex-direction: column; 
                                                    justify-content: space-between; 
                                                    padding: 15px; 
                                                    height: 600px;">
                                            <div class="panel-heading bg-default photo text-center"
                                                    style="height: 250px;">
                                                <div class="image-item">
                                                    <div class="image-wrapper">
                                                        
                                                    <?echo '<img src="data:image/jpeg;base64,'.base64_encode($gpuList[5]).'"/>'?>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="description panel-body">
                                                <h5><?= $gpuList[2]?></h5>
                                                <div class="properties row">
                                                    <strong>Данные:</strong>
                                                    <span>Производитель: <strong><?= $gpuList[1]?></strong></span>
                                                    <span>Память: <?= $gpuList[3]?> ГБ</span>
                                                    <span>Тип памяти: <?= $gpuList[4]?></span>
                                                    <span>Цена: <?= $gpuList[7]?> руб.</span>
                                                </div>
                                            </div>
                                            <div class="panel-footer actions text-center">
                                                <button type="submit" class="btn btn-add btn-block btn-secondary" id="btn-add-gpu"
                                                            href="" rel="nofollow"
                                                            data-id=""
                                                            style="width: 150px;"
                                                            data-id="<?php echo $gpuList;?>">
			                                        Выбрать		
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                <? endforeach; ?>    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Процессор -->
    <div class="modal fade webprostor-configirator-modal theme-light in" id="modal-cpu"
            tabindex="-1" aria-labelledby="modalProductsLabel" aria-hidden="true" 
            style="overflow-y: scroll;">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalProductsLabel">Процессор</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12 col-md-12">
                            <div class="webprostor-configurator-catalog-section">
                                <div class="row">

                                <? foreach ($cpu as $cpuList): ?> <!-- Цикл for из массива $.... Поле таблицы - $...List[цифра] 
                                    (отсчёт идёт с нуля: 0 - первое поле таблицы, 1 - второе поле и т.д.)  -->

                                    <!-- Карточка товара -->
                                    <div class="col-md-4 col-sm-6 col-xs-12 configurator-product mb-3">
                                        <div class="panel panel-default border rounded" 
                                                style="display: flex; 
                                                    flex-direction: column; 
                                                    justify-content: space-between; 
                                                    padding: 15px; 
                                                    height: 600px;">
                                            <div class="panel-heading bg-default photo text-center"
                                                    style="height: 250px; width: 300px;">
                                                <div class="image-item">
                                                    <div class="image-wrapper" style="height: 250px; width: 300px;">
                                                        
                                                    <?echo '<img src="data:image/jpeg;base64,'.base64_encode($cpuList[4]).'"/>'?>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="description panel-body">
                                                <h5><?= $cpuList[2]?></h5>
                                                <div class="properties row">
                                                    <strong>Данные:</strong>
                                                    <span>Производитель: <strong><?= $cpuList[1]?></strong></span>
                                                    <span>Сокет: <?= $cpuList[3]?></span>
                                                    <span><strong>Прочее:</strong> 
                                                        <?= $cpuList[5]?>
                                                    </span>
                                                    <span>Цена: <?= $cpuList[6]?> руб.</span>
                                                </div>
                                            </div>
                                            <div class="panel-footer actions text-center">
                                                <button type="submit" class="btn btn-add btn-block btn-secondary" id="btn-add-gpu"
                                                            href="" rel="nofollow"
                                                            data-id=""
                                                            style="width: 150px;">
			                                        Выбрать		
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                <? endforeach; ?>    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Мат. плата -->
    <div class="modal fade webprostor-configirator-modal theme-light in" id="modal-motherboard"
            tabindex="-1" aria-labelledby="modalProductsLabel" aria-hidden="true" 
            style="overflow-y: scroll;">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalProductsLabel">Материнская плата</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12 col-md-12">
                            <div class="webprostor-configurator-catalog-section">
                                <div class="row">

                                <? foreach ($motherboard as $motherboardList): ?> <!-- Цикл for из массива $.... Поле таблицы - $...List[цифра] 
                                    (отсчёт идёт с нуля: 0 - первое поле таблицы, 1 - второе поле и т.д.)  -->

                                    <!-- Карточка товара -->
                                    <div class="col-md-4 col-sm-6 col-xs-12 configurator-product mb-3">
                                        <div class="panel panel-default border rounded" 
                                                style="display: flex; 
                                                    flex-direction: column; 
                                                    justify-content: space-between; 
                                                    padding: 15px; 
                                                    height: 600px;">
                                            <div class="panel-heading bg-default photo text-center"
                                                    style="height: 250px;">
                                                <div class="image-item">
                                                    <div class="image-wrapper">
                                                        
                                                    <?echo '<img src="data:image/jpeg;base64,'.base64_encode($motherboardList[4]).'"/>'?>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="description panel-body">
                                                <h5><?= $motherboardList[2]?></h5>
                                                <div class="properties row">
                                                    <strong>Данные:</strong>
                                                    <span>Производитель: <strong><?= $motherboardList[1]?></strong></span>
                                                    <span>Сокет: <?= $motherboardList[3]?></span>
                                                    <span>Цена: <?= $motherboardList[6]?> руб.</span>
                                                </div>
                                            </div>
                                            <div class="panel-footer actions text-center">
                                                <button type="submit" class="btn btn-add btn-block btn-secondary" id="btn-add-gpu"
                                                            href="" rel="nofollow"
                                                            data-id=""
                                                            style="width: 150px;"
                                                            data-id="">
			                                        Выбрать		
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                <? endforeach; ?>    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Кулер для процессора -->
    <div class="modal fade webprostor-configirator-modal theme-light in" id="modal-cpu_cooler"
            tabindex="-1" aria-labelledby="modalProductsLabel" aria-hidden="true" 
            style="overflow-y: scroll;">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalProductsLabel">Кулер для процессора</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12 col-md-12">
                            <div class="webprostor-configurator-catalog-section">
                                <div class="row">

                                <? foreach ($cpu_cooler as $cpu_coolerList): ?> <!-- Цикл for из массива $.... Поле таблицы - $...List[цифра] 
                                    (отсчёт идёт с нуля: 0 - первое поле таблицы, 1 - второе поле и т.д.)  -->

                                    <!-- Карточка товара -->
                                    <div class="col-md-4 col-sm-6 col-xs-12 configurator-product mb-3">
                                        <div class="panel panel-default border rounded" 
                                                style="display: flex; 
                                                    flex-direction: column; 
                                                    justify-content: space-between; 
                                                    padding: 15px; 
                                                    height: 600px;">
                                            <div class="panel-heading bg-default photo text-center"
                                                    style="height: 250px;">
                                                <div class="image-item">
                                                    <div class="image-wrapper">
                                                        
                                                    <?echo '<img src="data:image/jpeg;base64,'.base64_encode($cpu_coolerList[4]).'"/>'?>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="description panel-body">
                                                <h5><?= $cpu_coolerList[2]?></h5>
                                                <div class="properties row">
                                                    <strong>Данные:</strong>
                                                    <span>Производитель: <strong><?= $cpu_coolerList[1]?></strong></span>
                                                    <span>Сокет: <?= $cpu_coolerList[3]?></span>
                                                    <span><strong>Прочее: </strong><?= $cpu_coolerList[5]?></span>
                                                    <span>Цена: <?= $cpu_coolerList[6]?> руб.</span>
                                                </div>
                                            </div>
                                            <div class="panel-footer actions text-center">
                                                <button type="submit" class="btn btn-add btn-block btn-secondary" id="btn-add-gpu"
                                                            href="" rel="nofollow"
                                                            data-id=""
                                                            style="width: 150px;"
                                                            data-id="">
			                                        Выбрать		
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                <? endforeach; ?>    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Водяное охлаждение процессора -->
    <div class="modal fade webprostor-configirator-modal theme-light in" id="modal-cpu_water_cooling"
            tabindex="-1" aria-labelledby="modalProductsLabel" aria-hidden="true" 
            style="overflow-y: scroll;">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalProductsLabel">Водяное охлаждение процессора</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12 col-md-12">
                            <div class="webprostor-configurator-catalog-section">
                                <div class="row">

                                <? foreach ($cpu_water_cooling as $cpu_water_coolingList): ?> <!-- Цикл for из массива $.... Поле таблицы - $...List[цифра] 
                                    (отсчёт идёт с нуля: 0 - первое поле таблицы, 1 - второе поле и т.д.)  -->

                                    <!-- Карточка товара -->
                                    <div class="col-md-4 col-sm-6 col-xs-12 configurator-product mb-3">
                                        <div class="panel panel-default border rounded" 
                                                style="display: flex; 
                                                    flex-direction: column; 
                                                    justify-content: space-between; 
                                                    padding: 15px; 
                                                    height: 600px;">
                                            <div class="panel-heading bg-default photo text-center"
                                                    style="height: 250px;">
                                                <div class="image-item">
                                                    <div class="image-wrapper">
                                                        
                                                    <?echo '<img src="data:image/jpeg;base64,'.base64_encode($cpu_water_coolingList[4]).'"/>'?>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="description panel-body">
                                                <h5><?= $cpu_water_coolingList[2]?></h5>
                                                <div class="properties row">
                                                    <strong>Данные:</strong>
                                                    <span>Производитель: <strong><?= $cpu_water_coolingList[1]?></strong></span>
                                                    <span>Сокет: <?= $cpu_water_coolingList[3]?></span>
                                                    <span><strong>Прочее: </strong><?= $cpu_water_coolingList[5]?></span>
                                                    <span>Цена: <?= $cpu_water_coolingList[6]?> руб.</span>
                                                </div>
                                            </div>
                                            <div class="panel-footer actions text-center">
                                                <button type="submit" class="btn btn-add btn-block btn-secondary" id="btn-add-gpu"
                                                            href="" rel="nofollow"
                                                            data-id=""
                                                            style="width: 150px;"
                                                            data-id="">
			                                        Выбрать		
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                <? endforeach; ?>    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Оперативная память -->
    <div class="modal fade webprostor-configirator-modal theme-light in" id="modal-ram"
            tabindex="-1" aria-labelledby="modalProductsLabel" aria-hidden="true" 
            style="overflow-y: scroll;">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalProductsLabel">Оперативная память</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12 col-md-12">
                            <div class="webprostor-configurator-catalog-section">
                                <div class="row">

                                <? foreach ($ram as $ramList): ?> <!-- Цикл for из массива $.... Поле таблицы - $...List[цифра] 
                                    (отсчёт идёт с нуля: 0 - первое поле таблицы, 1 - второе поле и т.д.)  -->

                                    <!-- Карточка товара -->
                                    <div class="col-md-4 col-sm-6 col-xs-12 configurator-product mb-3">
                                        <div class="panel panel-default border rounded" 
                                                style="display: flex; 
                                                    flex-direction: column; 
                                                    justify-content: space-between; 
                                                    padding: 15px; 
                                                    height: 600px;">
                                            <div class="panel-heading bg-default photo text-center"
                                                    style="height: 250px;">
                                                <div class="image-item">
                                                    <div class="image-wrapper">
                                                        
                                                    <?echo '<img src="data:image/jpeg;base64,'.base64_encode($ramList[5]).'"/>'?>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="description panel-body">
                                                <h5><?= $ramList[2]?></h5>
                                                <div class="properties row">
                                                    <strong>Данные:</strong>
                                                    <span>Производитель: <strong><?= $ramList[1]?></strong></span>
                                                    <span>Кол-во памяти: <?= $ramList[3]?> ГБ</span>
                                                    <span>Тип памяти: <?= $ramList[4]?></span>
                                                    <span>Цена: <?= $ramList[7]?> руб.</span>
                                                </div>
                                            </div>
                                            <div class="panel-footer actions text-center">
                                                <button type="submit" class="btn btn-add btn-block btn-secondary" id="btn-add-gpu"
                                                            href="" rel="nofollow"
                                                            data-id=""
                                                            style="width: 150px;"
                                                            data-id="">
			                                        Выбрать		
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                <? endforeach; ?>    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- SSD M.2 -->
    <div class="modal fade webprostor-configirator-modal theme-light in" id="modal-ssd_m2"
            tabindex="-1" aria-labelledby="modalProductsLabel" aria-hidden="true" 
            style="overflow-y: scroll;">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalProductsLabel">SSD M.2</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12 col-md-12">
                            <div class="webprostor-configurator-catalog-section">
                                <div class="row">

                                <? foreach ($ssd_m2 as $ssd_m2List): ?> <!-- Цикл for из массива $.... Поле таблицы - $...List[цифра] 
                                    (отсчёт идёт с нуля: 0 - первое поле таблицы, 1 - второе поле и т.д.)  -->

                                    <!-- Карточка товара -->
                                    <div class="col-md-4 col-sm-6 col-xs-12 configurator-product mb-3">
                                        <div class="panel panel-default border rounded" 
                                                style="display: flex; 
                                                    flex-direction: column; 
                                                    justify-content: space-between; 
                                                    padding: 15px; 
                                                    height: 600px;">
                                            <div class="panel-heading bg-default photo text-center"
                                                    style="height: 250px;">
                                                <div class="image-item">
                                                    <div class="image-wrapper">
                                                        
                                                    <?echo '<img src="data:image/jpeg;base64,'.base64_encode($ssd_m2List[4]).'"/>'?>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="description panel-body">
                                                <h5><?= $ssd_m2List[2]?></h5>
                                                <div class="properties row">
                                                    <strong>Данные:</strong>
                                                    <span>Производитель: <strong><?= $ssd_m2List[1]?></strong></span>                                                    
                                                    <span>Цена: <?= $ssd_m2List[6]?> руб.</span>
                                                </div>
                                            </div>
                                            <div class="panel-footer actions text-center">
                                                <button type="submit" class="btn btn-add btn-block btn-secondary" id="btn-add-gpu"
                                                            href="" rel="nofollow"
                                                            data-id=""
                                                            style="width: 150px;"
                                                            data-id="">
			                                        Выбрать		
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                <? endforeach; ?>    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- SSS накопитель -->
    <div class="modal fade webprostor-configirator-modal theme-light in" id="modal-ssd_sata"
            tabindex="-1" aria-labelledby="modalProductsLabel" aria-hidden="true" 
            style="overflow-y: scroll;">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalProductsLabel">SSD накопитель</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12 col-md-12">
                            <div class="webprostor-configurator-catalog-section">
                                <div class="row">

                                <? foreach ($ssd_sata as $ssd_sataList): ?> <!-- Цикл for из массива $.... Поле таблицы - $...List[цифра] 
                                    (отсчёт идёт с нуля: 0 - первое поле таблицы, 1 - второе поле и т.д.)  -->

                                    <!-- Карточка товара -->
                                    <div class="col-md-4 col-sm-6 col-xs-12 configurator-product mb-3">
                                        <div class="panel panel-default border rounded" 
                                                style="display: flex; 
                                                    flex-direction: column; 
                                                    justify-content: space-between; 
                                                    padding: 15px; 
                                                    height: 600px;">
                                            <div class="panel-heading bg-default photo text-center"
                                                    style="height: 250px;">
                                                <div class="image-item">
                                                    <div class="image-wrapper">
                                                        
                                                    <?echo '<img src="data:image/jpeg;base64,'.base64_encode($ssd_sataList[4]).'"/>'?>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="description panel-body">
                                                <h5><?= $ssd_sataList[2]?></h5>
                                                <div class="properties row">
                                                    <strong>Данные:</strong>
                                                    <span>Производитель: <strong><?= $ssd_sataList[1]?></strong></span>      
                                                    <span>Кол-во памяти: <?= $ssd_sataList[3]?> ГБ</span>                                              
                                                    <span>Цена: <?= $ssd_sataList[6]?> руб.</span>
                                                </div>
                                            </div>
                                            <div class="panel-footer actions text-center">
                                                <button type="submit" class="btn btn-add btn-block btn-secondary" id="btn-add-gpu"
                                                            href="" rel="nofollow"
                                                            data-id=""
                                                            style="width: 150px;"
                                                            data-id="">
			                                        Выбрать		
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                <? endforeach; ?>    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- HDD -->
    <div class="modal fade webprostor-configirator-modal theme-light in" id="modal-hdd"
            tabindex="-1" aria-labelledby="modalProductsLabel" aria-hidden="true" 
            style="overflow-y: scroll;">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalProductsLabel">Жесткий диск</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12 col-md-12">
                            <div class="webprostor-configurator-catalog-section">
                                <div class="row">

                                <? foreach ($hdd as $hddList): ?> <!-- Цикл for из массива $.... Поле таблицы - $...List[цифра] 
                                    (отсчёт идёт с нуля: 0 - первое поле таблицы, 1 - второе поле и т.д.)  -->

                                    <!-- Карточка товара -->
                                    <div class="col-md-4 col-sm-6 col-xs-12 configurator-product mb-3">
                                        <div class="panel panel-default border rounded" 
                                                style="display: flex; 
                                                    flex-direction: column; 
                                                    justify-content: space-between; 
                                                    padding: 15px; 
                                                    height: 600px;">
                                            <div class="panel-heading bg-default photo text-center"
                                                    style="height: 250px;">
                                                <div class="image-item">
                                                    <div class="image-wrapper">
                                                        
                                                    <?echo '<img src="data:image/jpeg;base64,'.base64_encode($hddList[4]).'"/>'?>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="description panel-body">
                                                <h5><?= $hddList[2]?></h5>
                                                <div class="properties row">
                                                    <strong>Данные:</strong>
                                                    <span>Производитель: <strong><?= $hddList[1]?></strong></span>      
                                                    <span>Кол-во памяти: <?= $hddList[3]?> ГБ</span>                                              
                                                    <span>Цена: <?= $hddList[6]?> руб.</span>
                                                </div>
                                            </div>
                                            <div class="panel-footer actions text-center">
                                                <button type="submit" class="btn btn-add btn-block btn-secondary" id="btn-add-gpu"
                                                            href="" rel="nofollow"
                                                            data-id=""
                                                            style="width: 150px;"
                                                            data-id="">
			                                        Выбрать		
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                <? endforeach; ?>    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Блок питания -->
    <div class="modal fade webprostor-configirator-modal theme-light in" id="modal-power_supply"
            tabindex="-1" aria-labelledby="modalProductsLabel" aria-hidden="true" 
            style="overflow-y: scroll;">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalProductsLabel">Блок питания</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12 col-md-12">
                            <div class="webprostor-configurator-catalog-section">
                                <div class="row">

                                <? foreach ($power_supply as $power_supplyList): ?> <!-- Цикл for из массива $.... Поле таблицы - $...List[цифра] 
                                    (отсчёт идёт с нуля: 0 - первое поле таблицы, 1 - второе поле и т.д.)  -->

                                    <!-- Карточка товара -->
                                    <div class="col-md-4 col-sm-6 col-xs-12 configurator-product mb-3">
                                        <div class="panel panel-default border rounded" 
                                                style="display: flex; 
                                                    flex-direction: column; 
                                                    justify-content: space-between; 
                                                    padding: 15px; 
                                                    height: 600px;">
                                            <div class="panel-heading bg-default photo text-center"
                                                    style="height: 250px;">
                                                <div class="image-item">
                                                    <div class="image-wrapper">
                                                        
                                                    <?echo '<img src="data:image/jpeg;base64,'.base64_encode($power_supplyList[4]).'"/>'?>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="description panel-body">
                                                <h5><?= $power_supplyList[2]?></h5>
                                                <div class="properties row">
                                                    <strong>Данные:</strong>
                                                    <span>Производитель: <strong><?= $power_supplyList[1]?></strong></span>      
                                                    <span>Мощность: <?= $power_supplyList[3]?> Вт</span>                                              
                                                    <span>Цена: <?= $power_supplyList[6]?> руб.</span>
                                                </div>
                                            </div>
                                            <div class="panel-footer actions text-center">
                                                <button type="submit" class="btn btn-add btn-block btn-secondary" id="btn-add-gpu"
                                                            href="" rel="nofollow"
                                                            data-id=""
                                                            style="width: 150px;"
                                                            data-id="">
			                                        Выбрать		
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                <? endforeach; ?>    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Корпус -->
    <div class="modal fade webprostor-configirator-modal theme-light in" id="modal-body"
            tabindex="-1" aria-labelledby="modalProductsLabel" aria-hidden="true" 
            style="overflow-y: scroll;">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalProductsLabel">Корпус</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12 col-md-12">
                            <div class="webprostor-configurator-catalog-section">
                                <div class="row">

                                <? foreach ($body as $bodyList): ?> <!-- Цикл for из массива $.... Поле таблицы - $...List[цифра] 
                                    (отсчёт идёт с нуля: 0 - первое поле таблицы, 1 - второе поле и т.д.)  -->

                                    <!-- Карточка товара -->
                                    <div class="col-md-4 col-sm-6 col-xs-12 configurator-product mb-3">
                                        <div class="panel panel-default border rounded" 
                                                style="display: flex; 
                                                    flex-direction: column; 
                                                    justify-content: space-between; 
                                                    padding: 15px; 
                                                    height: 600px;">
                                            <div class="panel-heading bg-default photo text-center"
                                                    style="height: 250px;">
                                                <div class="image-item">
                                                    <div class="image-wrapper">
                                                        
                                                    <?echo '<img src="data:image/jpeg;base64,'.base64_encode($bodyList[3]).'"/>'?>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="description panel-body">
                                                <h5><?= $bodyList[2]?></h5>
                                                <div class="properties row">
                                                    <strong>Данные:</strong>
                                                    <span>Производитель: <strong><?= $bodyList[1]?></strong></span>
                                                    <span>Цена: <?= $bodyList[5]?> руб.</span>
                                                </div>
                                            </div>
                                            <div class="panel-footer actions text-center">
                                                <button type="submit" class="btn btn-add btn-block btn-secondary" 
                                                            href="" rel="nofollow"
                                                            data-id=""
                                                            style="width: 150px;">
			                                        Выбрать		
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                <? endforeach; ?>    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Wi-Fi -->
    <div class="modal fade webprostor-configirator-modal theme-light in" id="modal-wi_fi"
            tabindex="-1" aria-labelledby="modalProductsLabel" aria-hidden="true" 
            style="overflow-y: scroll;">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalProductsLabel">Wi-Fi адаптер</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12 col-md-12">
                            <div class="webprostor-configurator-catalog-section">
                                <div class="row">

                                <? foreach ($wi_fi as $wi_fiList): ?> <!-- Цикл for из массива $.... Поле таблицы - $...List[цифра] 
                                    (отсчёт идёт с нуля: 0 - первое поле таблицы, 1 - второе поле и т.д.)  -->

                                    <!-- Карточка товара -->
                                    <div class="col-md-4 col-sm-6 col-xs-12 configurator-product mb-3">
                                        <div class="panel panel-default border rounded" 
                                                style="display: flex; 
                                                    flex-direction: column; 
                                                    justify-content: space-between; 
                                                    padding: 15px; 
                                                    height: 600px;">
                                            <div class="panel-heading bg-default photo text-center"
                                                    style="height: 250px;">
                                                <div class="image-item">
                                                    <div class="image-wrapper">
                                                        
                                                    <?echo '<img src="data:image/jpeg;base64,'.base64_encode($wi_fiList[3]).'"/>'?>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="description panel-body">
                                                <h5><?= $wi_fiList[2]?></h5>
                                                <div class="properties row">
                                                    <strong>Данные:</strong>
                                                    <span>Производитель: <strong><?= $wi_fiList[1]?></strong></span>                                                 
                                                    <span>Цена: <?= $wi_fiList[5]?> руб.</span>
                                                </div>
                                            </div>
                                            <div class="panel-footer actions text-center">
                                                <button type="submit" class="btn btn-add btn-block btn-secondary" id="btn-add-gpu"
                                                            href="" rel="nofollow"
                                                            data-id=""
                                                            style="width: 150px;"
                                                            data-id="">
			                                        Выбрать		
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                <? endforeach; ?>    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.6.3.min.js" 
    integrity="sha256-pvPw+upLPUjgMXY0G+8O0xUf+/Im1MZjXxxgOcBQBXU=" 
    crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" 
    integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" 
    crossorigin="anonymous"></script>
    <script src="/js/scripts.js"></script>

</body>
</html>