<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Конструктор ПК</title>

    <!-- Bootstap -->
    <!-- <link rel="stylesheet" href="/bootstrap-5.0.2-dist/css/bootstrap.min.css"> -->
    <!-- <script defer src="/bootstrap-5.0.2-dist/js/bootstrap.bundle.min.js"></script> -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" 
    rel="stylesheet" 
    integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" 
    crossorigin="anonymous">

    <!-- Стили -->
    <link rel="stylesheet" type="text/css" href="/css/custom.css">

</head>
<body>

    <header>
        <nav class="navbar navbar-expand-lg bg-body-tertiary" style="background-color: #dedede;">
            <div class="container-fluid">
                <a href="index.php" class="navbar-brand" style="color: black;">
                    <img src="/resource/logo.png" alt="Logo" width="30" height="30" class="d-inline-block align-text-top">
                    Конструктор ПК
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Переключатель навигации">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                    <div class="navbar-nav ">
                        <a class="nav-link navigation" href="main.php">Главная</a>
                        <a class="nav-link navigation" href="index.php">Конструктор</a>
                    </div>
                </div>
            </div>
        </nav>
    </header>
    <main>
        <div class="">
            <strong>Добро пожаловать!</strong>
            <p>
                Это "Конструктор ПК", котором можно собрать конфигурацию из представленных комплектующих
            </p>
        </div>
    </main>
    <footer class="footer" id="footer">
        <nav class="navbar navbar-expand-lg bg-body-tertiary" style="background-color: #252525;">
            <div class="container-fluid">
                <a href="index.php" class="navbar-brand" style="color: #ffff;">
                    Конструктор ПК
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Переключатель навигации">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                    <div class="navbar-nav">
                        <a class="nav-link navigation" href="main.php" style="color: #ffff;">Главная</a>
                        <a class="nav-link navigation" href="index.php" style="color: #ffff;">Конструктор</a>
                    </div>
                </div>
            </div>
        </nav>
    </footer>

    <script src="https://code.jquery.com/jquery-3.6.3.min.js" 
    integrity="sha256-pvPw+upLPUjgMXY0G+8O0xUf+/Im1MZjXxxgOcBQBXU=" 
    crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" 
    integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" 
    crossorigin="anonymous"></script>

</body>
</html>