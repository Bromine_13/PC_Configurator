<?php

define('DBSERVER', 'localhost'); // Сервер базы данных
define('DBUSERNAME', 'root'); // Пользователь базы данных
define('DBPASSWORD', 'root'); // Пароль базы данных
define('DBNAME', 'assembly_constructor'); // Название базы данных
 
/* Подключение к базе данных */
$db = mysqli_connect(DBSERVER, DBUSERNAME, DBPASSWORD, DBNAME);
 
/* Проверка подключения к базе данных */
if($db === false){
    die("Error: connection error. " . mysqli_connect_error());
}

?>